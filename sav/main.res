Turn #1
p1 attacks p2
p1 fires phaser (dmg: 25)
p2 uses shield (cd: 5/5) and absorbs 25 damages
p1 fires blaster (dmg: 50, cd: 3/3)
p2 attacks p1
p2 fires phaser (dmg: 10)
p1 uses absorber (abs: 5) and absorbs 5 damages
p1 uses solar panel (energy: 10)
p1 uses robots (hull: 10)
End turn #1
 * p1's ship (hull: 100, energy: 200)
 * p2's ship (hull: 50, energy: 197)

Turn #2
p1 attacks p2
p1 fires phaser (dmg: 25)
p2 attacks p1
p2 fires phaser (dmg: 10)
p1 uses absorber (abs: 5) and absorbs 5 damages
p1 uses solar panel (energy: 10)
p1 uses robots (hull: 10)
End turn #2
 * p1's ship (hull: 100, energy: 200)
 * p2's ship (hull: 25, energy: 196)

Turn #3
p1 attacks p2
p1 fires phaser (dmg: 25)
End turn #3
 * p1's ship (hull: 100, energy: 198)
 * p2's ship (hull: 0, energy: 196)

Game over: p1 wins!
